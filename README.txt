  -----------------------------------------------------------------------------------------
                                    ABOUT THE MODULE
  -----------------------------------------------------------------------------------------
  This module intergrates Seevolution analytics service into Drupal
  
  
  
  -----------------------------------------------------------------------------------------
                                    HOW TO USE NODE CLASS
  -----------------------------------------------------------------------------------------
	1) Go to http://www.seevolution.com and create your new account
	2) Enable the module from admin/build/modules
	3) Take the code snippet they give you and paste it inside the proper textarea in admin/settings/seevolution
	4) You will notice that an icon on the left bottom corner of your website appears, click on it to access your seevolution data
	